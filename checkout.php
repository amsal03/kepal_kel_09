<?php
include('listFungsi.php');
doHtmlHeader('List Produk');

$total = 0; 
$conn = doConnect();
$id = $_GET['ID_Keranjang'];
    $user = getuserId();
    $de = $conn->query("SELECT * FROM keranjang WHERE ID_Keranjang = '".$_GET['ID_Keranjang']."'");
    $detail = $de->fetch_assoc();

    $query = "SELECT * FROM keranjang_barang JOIN produk ON keranjang_barang.ID_Produk = produk.ID_Produk
                WHERE keranjang_barang.ID_Keranjang = '".$_GET['ID_Keranjang']."'";
    $hasil = mysqli_query($conn, $query);
if(mysqli_num_rows($hasil)>0){
?> 
<div class="container">
        <div class="checkout-section">
            <div>
                <form action='actionCheckout.php' method='post'>
                    <?php echo '<input type="hidden" class="form-control" id="idd" name="idd" value="'.$id.'">';?>
                    <h2>Detail Pembayaran</h2>
                    <div class="form-group">
                        <label for="name">Name</label>
                        <input type="text" class="form-control" id="name" name="name" value="" required>
                    </div>
                    <div class="form-group">
                        <label for="address">Address</label>
                        <input type="text" class="form-control" id="address" name="address" value="" required>
                    </div>

                    <div class="half-form">
                        <div class="form-group">
                            <label for="city">City</label>
                            <input type="text" class="form-control" id="city" name="city" value="" required>
                        </div>
                        <div class="form-group">
                            <label for="province">Province</label>
                            <input type="text" class="form-control" id="province" name="province" value="" required>
                        </div>
                    </div> <!-- end half-form -->

                    <div class="half-form">
                        <div class="form-group">
                            <label for="postalcode">Postal Code</label>
                            <input type="text" class="form-control" id="postalcode" name="postalcode" required>
                        </div>
                        <div class="form-group">
                            <label for="phone">Phone</label>
                            <input type="text" class="form-control" id="phone" name="phone" value="" required=>
                        </div>
                    </div> <!-- end half-form -->

                    <div class="spacer"></div>
                    
                    <div class="spacer"></div>
                    
                    <input type="submit" class="flex-c-m sizefull bg1 bo-rad-23 hov1 s-text1 trans-0-4" value="Complete Order">
                </form>
            </div>



            <div class="checkout-table-container">
                <h2>Pesanan</h2>

                <div class="checkout-table">
                <?php   
                    while($data = mysqli_fetch_array($hasil)){
                    //$total = $total + ($data['Harga'] * $data['Jumlah_Produk']);
                ?>
                    <div class="checkout-table-row">
                        <div class="checkout-table-row-left">
                            <?php echo "<img src='img/".$data['Gambar']."' alt='item' width='100' height= 0px >"?>
                            <div class="checkout-item-details">
                                <div class="checkout-table-item"><?php echo $data['Nama'];?></div>
                                <div class="checkout-table-description"><?pshp echo $data['Deskripsi'];?></div>
                                <div class="checkout-table-price">Rp <?php echo $data['Harga'];?>,-</div>
                            </div>
                        </div> <!-- end checkout-table -->

                        <div class="checkout-table-row-right">
                            <div class="checkout-table-quantity"><?php echo number_format($data['Jumlah']);?></div>
                        </div>
                    </div> <!-- end checkout-table-row -->
<?php }?>
                </div>

                <div class="checkout-totals">
                    <div class="checkout-totals-left">
                        <span class="checkout-totals-total">Total</span>
                    </div>
                    <div class="checkout-totals-right">
                        <span class="checkout-totals-total">Rp <?php echo number_format($detail['total']);?>,-</span>
                    </div>
                </div>  
            </div>

        </div> <!-- end checkout-section -->
    </div>
<?php } 

doHtmlFooter();
?>