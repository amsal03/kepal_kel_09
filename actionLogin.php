<?php
include_once("listFungsi.php");

doHtmlHeader("Log in");

$username = getParam("username");
$password = getParam("password");
if(doAutentikasi($username,$password) ) {
	$ID = getUserId();
	if($ID == 1 ){
		goToURL("backend/pages/index.php");
	}else{
		goToURL("Home.php");	
	}
	
}else{
	goToURL("Login.php?pesan=gagal");
}

doHtmlFooter();
?>