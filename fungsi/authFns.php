<?php
//File : authFns.php

function isValidUser ($username,$password) {
	$passwords = $password;
	//cek apakah username tersebut ada di databae
	$user = getUser($username);
	if($user) {
		//jika ada cek apakah password yang diberikan valid
		if($user["Password"] == $passwords) {
			return $user;
		}else{
			return false;
		}
	}else{
		return false;
	}
}

function doAutentikasi($username,$password) {
	if($user = isValidUser ($username,$password) ) {
		setSessionParam("apps_user",serialize($user) );
		return true;
	}else{
		return false;
	}
}

function isLogin() {
	if(isset($_SESSION["apps_user"])) 
		return true;
	else return false;
}

function getUserId() {
	if(isset($_SESSION["apps_user"])) {
		$user = unserialize(getSessionParam("apps_user"));
		return $user["ID_User"];
	}else return "guest";
}

function doLogout() {
	@session_start();
	// Unset all of the session variables.
	session_unset();
	// Finally, destroy the session.
	session_destroy();
}
?>