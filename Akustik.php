<?php
include('listFungsi.php');
doHtmlHeader('List Produk');
$db = doConnect();
$user = getuserId();
$limit = 12;
$jumlah_record = mysqli_query($db, "SELECT * from produk WHERE jenis = 1");
$jum = mysqli_num_rows($jumlah_record);
$halaman = ceil($jum / $limit);
$page = (isset($_GET['page'])) ? (int)$_GET['page'] : 1;
$start = ($page - 1) * $limit;
?>

<div class="featured-section">
            <div class="breadcrumbs">
                <div class="container">
                    <a href="#">Home</a>
                    <i class="fa fa-chevron-right breadcrumb-separator"></i>
                    <span>Product</span>
                </div>
            </div> <!-- end breadcrumbs -->

            <div class="products-section container">
                <div class="sidebar">
                    <h3>By Category</h3>
                    <ul>
                        <li><a href="Akustik.php">Gitar Akustik</a></li>
                        <li><a href="Listrik.php">Gitar Listrik</a></li>
                        <li><a href="Bass.php">Gitar Bass</a></li>
                    </ul>
                </div> 

                <div>
                    <h1 class="stylish-heading">Gitar Akustik</h1>
                    <div class="products text-center">
                        
                        <?php   
                        $gitar = mysqli_query($db,"SELECT * FROM produk WHERE jenis=1 LIMIT $start, $limit" );

                         while($data = mysqli_fetch_array($gitar)){
                        ?>          
                    
                     <div class="product">
                         <table>
                        <tr>
                            <td> <?php echo "<a href='Produk.php?ID_Produk=".$data['ID_Produk']."'><img src='img/".$data['Gambar']."' alt='item' class='products-table-img'>" ?></td>
                        </tr>
                        <tr>
                            <td><div class="product-name1"> <?php echo $data['Nama'];?></div></td>
                        </tr>
                        <tr>
                            <td><div class="product-price1">Rp.<?php echo number_format($data['Harga']);?></div></td>
                        </tr>
                        </div> 
                  
                    </table> 
                     </div>
                <?php }?>
                </div> 
                 <br><br>
                <ul class="pagination" style="margin-left: 610px;">         
                     <?php 
                     for($x=1; $x<=$halaman; $x++){
                        ?>
                        <li><a href="?page=<?php echo $x ?>"><?php echo $x ?></a></li>
                    <?php
                     }
                    ?>   
                </ul>
            </div>
        </div>

<?php 
doHtmlFooter(); 
