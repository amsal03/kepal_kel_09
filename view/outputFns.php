<?php
//File : outpunFns.php


function doHtmlHeader($title) {
	global $config;
	@session_start();

	//anda bisa menaruh potongan header html Anda
	//atau mengincludekan potongan tersebut
	include("layout/top.php");
}

function doHtmlFooter() {
	global $config;
	//anda bisa menaruh potongan footer html Anda
	//atau mengincludekan potongan tersebut

	include("layout/bottom.php");
}

function doHtmlUrl($url,$title) {
	echo " <a href='$url'>$title</a> ";
}

function printErr($str) {
	echo "<p><font color=#ff0000><b>$str</font></b></p>";
}

function diplayImage($nama_file,$url=null,$size) {
	if(file_exists($nama_file)) {
		if ($url) {
			echo "<a href='$url'><img src='$nama_file' width=$size border=0></a>";
		}else{
			echo "<img src='$nama_file' width=$size border=0>";
		}
	}
}


//Form log in
//action ke admin.php
function displayLoginForm() {
	if(isset($_GET['pesan'])){
		echo '<br><br><br><br><br><section class="login-block">
            <div class="container">
                <div class="row">
                <div class="col-md-7 ">
                </div>
                    <div class="col-md-4">
                        <h2 class="text-center">Login Now</h2>
                        <center><a style="color: red">Username atau password anda salah<br> silahkan coba lagi</a></center>
                          <form class="login-form" method="post" action="actionLogin.php">
                            <div class="form-group">
                              <label for="exampleInputEmail1" class="text-uppercase">Username</label>
                              <input type="text" class="form-control" placeholder="" name="username" required>
                            </div>
                            <div class="form-group">
                              <label for="exampleInputPassword1" class="text-uppercase">Password</label>
                              <input type="password" class="form-control" placeholder="" name="password" required>
                            </div>
                            <div class="form-check">
                              <label class="form-check-label">
                                <input type="checkbox" class="form-check-input">
                                <small>Remember Me</small>
                              </label>
                              <button type="submit" class="btn btn-primary float-right">Submit</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </section><br><br><br>';

	}else{
		echo '
		<br><br><br><br><section class="login-block">
            <div class="container">
                <div class="row">
                    <div class="col-md-7 ">
                    </div>
                    <div class="col-md-4 login-sec"><br><br>
                        <h2 class="text-center">Login Now</h2><br><br>
                        <form class="login-form" method=post action = "actionLogin.php">
                          <div class="form-group">
                            <label for="exampleInputEmail1" class="text-uppercase">Username</label>
                            <input type="text" class="form-control" placeholder="" name="username" required>
                          </div>
                          <div class="form-group">
                            <label for="exampleInputPassword1" class="text-uppercase">Password</label>
                            <input type="password" class="form-control" placeholder="" name="password" required >
                          </div>
                            <div class="form-check">
                            <label class="form-check-label">
                              <input type="checkbox" class="form-check-input">
                              <small>Remember Me</small>
                            </label>
                            <button type="submit" class="btn btn-primary float-right">Submit</button>
                          </div>
                        </form>
                    </div>
                </div>
            </div>
        </section><br><br><br>  <br><br><br>
	';
	}	
}

function goToURL($url,$op=null) {
	echo "<SCRIPT>";
	echo "document.location = \"".getURL($url,$op)."\"";
	echo "</SCRIPT>";
//	header("Location:" . $url);
}

function getURL($url,$params=null) {
	if ($params && is_array($params)) {
		$url = "$url/?";
		while (list($key,$value) = each($params)) {
			$url .= "&$key=$value";
		}
	}
	return $url;
}

?>