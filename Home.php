<?php
include('listFungsi.php');
doHtmlHeader('Home');
$db = doConnect();
$user = getuserId();
$limit = 8;
/*$query = "SELECT * FROM produk ";*/
$jumlah_record = mysqli_query($db, "SELECT * FROM produk ");
$jum = mysqli_num_rows($jumlah_record);
$halaman = ceil($jum / $limit);
$page = (isset($_GET['page'])) ? (int)$_GET['page'] : 1;
$start = ($page - 1) * $limit;
?>
 <link rel="stylesheet" type="text/css" href="css/util.css">
	<!-- Slide1 -->
	<section class="slide1">
		<div class="wrap-slick1">
			<div class="slick1">
				<div class="item-slick1 item2-slick1" style="background-image: url(img/karisea.jpg);">
					<div class="wrap-content-slide1 sizefull flex-col-c-m p-l-15 p-r-15 p-t-150 p-b-170">
						<span class="caption1-slide1 m-text1 t-center animated visible-false m-b-15" data-appear="rollIn">
							Gitar buatan tangan khas Sipoholon, Tapanuli Utara
						</span>
						<h2 class="caption2-slide1 xl-text1 t-center animated visible-false m-b-37" data-appear="lightSpeedIn">
							Gitar Sipoholon
						</h2>
						
						<div class="wrap-btn-slide1 w-size1 animated visible-false" data-appear="slideInUp">
							<!-- Button -->
							<a href="product.html" class="flex-c-m size2 bo-rad-23 s-text2 bgwhite hov1 trans-0-4">
								Tentang Kami
							</a>
						</div>
					</div>
				</div>

				<div class="item-slick1 item3-slick1" style="background-image: url(img/antique.jpg);">
					<div class="wrap-content-slide1 sizefull flex-col-c-m p-l-15 p-r-15 p-t-150 p-b-170">
						<h2 class="caption2-slide1 xl-text1 t-center animated visible-false m-b-37" data-appear="rotateInUpRight">
							Gitar Sipoholon
						</h2>
						<span class="caption1-slide1 m-text1 t-center animated visible-false m-b-15" data-appear="rotateInDownLeft" >
							Dibuat dengan bahan lokal dan dengan cara yang tradisional
						</span>

						<div class="wrap-btn-slide1 w-size1 animated visible-false" data-appear="rotateIn">
							<!-- Button -->
							<a href="product.html" class="flex-c-m size2 bo-rad-23 s-text2 bgwhite hov1 trans-0-4">
								Tentang Kami
							</a>
						</div>
					</div>
				</div>

			</div>
		</div>
	</section>

	<!-- New Product -->
	<section class="banner bgwhite p-t-40 p-b-40">
		<div class="tab01">
            <div class="tab-content p-t-39">
				<h3 class="m-text5 t-center">
					Featured Products
				</h3>
				<div class="tab-pane fade show active" id="best-seller" role="tabpanel">
					<div class="row">
							<?php 
                         $gitar = mysqli_query($db,"SELECT * FROM produk LIMIT $start, $limit");
                         while($data = mysqli_fetch_array($gitar)){
                        ?>
							<div class="col-sm-6 col-md-4 col-lg-3 p-b-50 hov-img-zoom">
                            <!-- Block2 -->
                            <div class="block2">
                                <div class="block2-img wrap-pic-w of-hidden pos-relative">
                                    <?php echo "<a href='Produk.php?ID_Produk=".$data['ID_Produk']."'><img src='img/".$data['Gambar']."' alt='IMG_PRODUCT' >" ?></a>
                                </div>

                                <div class="block2-txt p-t-20 t-center  ">
                                    <?php 
                                    echo '<a href="Produk.php?ID_Produk='.$data['ID_Produk'].'" class="block2-name dis-block s-text3 p-b-5">';
                                        echo $data['Nama'];?>
                                    </a>
                                    <span class="block2-newprice m-text8 p-r-5">
                                        Rp.<?php echo number_format($data['Harga']);?>
                                    </span>

                                </div> 
                            </div>
                        </div>
                        <?php
                        }
                        ?>
                    </div>
                    <div class="text-center">
                   		<a href="Produks.php" class="button">View more products</a>
                	</div> 
                </div>
            </div>
        </div>

	</section>

	<!-- Blog -->
	<section class="blog bg5 p-t-94 p-b-65">
		<div class="container">
			<div class="sec-title p-b-52">
				<h3 class="m-text5 t-center">
					Customize Gitar 
				</h3>
			</div>

			<div class="row">
				<div class="col-sm-10 col-md-4 p-b-30 m-l-r-auto">
<!--Block3 --> 
					<div class="block3">
						<a href="custom.php" class="block3-img dis-block hov-img-zoom">
							<img src="img/gitar2.png" alt="IMG-BLOG">
						</a>

						<!-- <div class="block3-txt p-t-14">
							<h4 class="p-b-7">
								<a href="blog-detail.html" class="m-text11">
									Kustomis
								</a>
							</h4>

							<span class="s-text6">By</span> <span class="s-text7">Nancy Ward</span>
							<span class="s-text6">on</span> <span class="s-text7">July 22, 2017</span>

							<p class="s-text8 p-t-16">
								Duis ut velit gravida nibh bibendum commodo. Sus-pendisse pellentesque mattis augue id euismod. Inter-dum et malesuada fames
							</p>
						</div>  -->
					</div>
				</div>

				<div class="col-sm-10 col-md-4 p-b-30 m-l-r-auto">
					<!--Block3 --> 
					<div class="block3">
						<a href="custom.php" class="block3-img dis-block hov-img-zoom">
							<img src="img/gitar3.png" alt="IMG-BLOG">
						</a>
					</div>
				</div>

				<div class="col-sm-10 col-md-4 p-b-30 m-l-r-auto">
				<!-- Block3 --> 
					<div class="block3">
						<a href="custom.php" class="block3-img dis-block hov-img-zoom">
							<img src="img/gitar2.png" alt="IMG-BLOG">
						</a>
					</div>
				</div>

				<div class="col-sm-10 col-md-4 p-b-30 m-l-r-auto">
					<!--Block3 --> 
					<div class="block3">
						<a href="custom.php" class="block3-img dis-block hov-img-zoom">
							<img src="img/gitar3.png" alt="IMG-BLOG">
						</a>
					</div>
				</div>

				<div class="col-sm-10 col-md-4 p-b-30 m-l-r-auto">
				<!-- Block3 --> 
					<div class="block3">
						<a href="custom.php" class="block3-img dis-block hov-img-zoom">
							<img src="img/gitar2.png" alt="IMG-BLOG">
						</a>
					</div>
				</div>

				<div class="col-sm-10 col-md-4 p-b-30 m-l-r-auto">
				<!-- Block3 --> 
					<div class="block3">
						<a href="custom.php" class="block3-img dis-block hov-img-zoom">
							<img src="img/gitar3.png" alt="IMG-BLOG">
						</a>
					</div>
				</div>
			</div>
		</div>
	</section>

	<!-- Shipping -->
	<section class="shipping bgwhite p-t-62 p-b-46">
		<div class="flex-w p-l-15 p-r-15">
			<div class="flex-col-c w-size5 p-l-15 p-r-15 p-t-16 p-b-15 respon1">
				<h4 class="m-text12 t-center">
					Pengiriman barang dengan kurir terpercaya
				</h4>
				<span class="s-text11 t-center">
					JNE dan J&T
				</span>
			</div>

			<div class="flex-col-c w-size5 p-l-15 p-r-15 p-t-16 p-b-15 bo2 respon2">
				<h4 class="m-text12 t-center">
					Batas maksimum 21 hari
				</h4>

				<span class="s-text11 t-center">
					Apabila dalam jangka 21 hari barang belum sampai, uang akan dikembalikan.
				</span>
			</div>

			<div class="flex-col-c w-size5 p-l-15 p-r-15 p-t-16 p-b-15 respon1">
				<h4 class="m-text12 t-center">
					Open/Close Orderan
				</h4>

				<span class="s-text11 t-center">
					Orderan buka dari Senin sampai Sabtu
				</span>
			</div>
		</div>
	</section>
<?php
doHtmlFooter();
?>