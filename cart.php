<?php
session_start();
include('listFungsi.php');
doHtmlHeader('Cart');
$conn = doConnect();
$user = getuserId();
$total = 0;
?>
<script type="text/javascript">var x = 0;</script>

<!-- Cart -->
<?php if(isset($_SESSION["keranjang"]) && !empty($_SESSION['keranjang'])){
    ?>
    <br><br><br>
    <form method="POST" id="form">
        <section class="cart bgwhite p-t-70 p-b-100">
        <div class="container">
            <!-- Cart item -->
            <div class="container-table-cart pos-relative">
                <div class="wrap-table-shopping-cart bgwhite">
                    <table class="table-shopping-cart">
                        <tr class="table-head">
                            <th class="column-1"></th>
                            <th class="column-2">Produk</th>
                            <th class="column-3">Harga</th>
                            <th class="column-4 p-l-70">Jumlah</th>
                            <th class="column-5">Total</th>
                            <th class="column-6 p-l-70"></th>
                        </tr>
                        <?php   
                            $counterProduk = 0;
                            foreach ($_SESSION["keranjang"] as $ID_Produk => $jumlah) {
                                $test = $conn->query("SELECT * FROM produk WHERE ID_Produk = '".$ID_Produk."'");
                                $data = $test->fetch_assoc();
                                $total = $total + ($data['Harga'] * $jumlah);
                        ?>
                        <tr class="table-row">
                            <td class="column-1">
                                <div class="cart-img-product b-rad-4 o-f-hidden">
                                    <img src='img/<?=$data['Gambar']?>' alt='item' class='cart-table-img'>
                                </div>
                            </td>
                            <td class="column-2"><?=$data['Nama'];?></td>
                            <td class="column-3" id = "harga-<?= $data['ID_Produk']; ?>"><?= $data['Harga']; ?></td>
                            <td class="column-4 p-l-30">
                                <div class="flex-w bo5 of-hidden w-size17">
                                        <span class="btn-num-product-down color1 flex-c-m size7 bg8 eff2">
                                            <button id="minus-<?= $data['ID_Produk']; ?>" type="button" onclick="minus(this)" class="btn btn-default btn-number">
                                                <i class="fs-12 fa fa-minus" aria-hidden="true">-</i>
                                            </button>
                                        </span>

                                        <input type="text" name="form-<?= $data['ID_Produk']?>" class="size8 m-text18 t-center num-product" value="<?php echo $jumlah?>" min="1" max="10" id = "jumlah-<?= $data['ID_Produk']?>">

                                        <span class="btn-num-product-down color1 flex-c-m size7 bg8 eff2">
                                            <button id="plus-<?= $data['ID_Produk']; ?>" type="button" onclick="plus(this)" class="btn btn-default btn-number">
                                                <i class="fs-12 fa fa-plus" aria-hidden="true">+</i>
                                            </button>
                                        </span>
                                    </div>
                                </div>
                            </td>
                            <td class="column-5" id = "harga-terkali-<?= $data['ID_Produk']; ?>"><?= $data['Harga']; ?></td>
                            <td class="column-6 p-l-70"><?php echo "<a class='text-danger' href='Remove.php?ID_Produk=".$data['ID_Produk']."'>Remove</a>" ?> <br></td>
                        </tr>
                        <?php $counterProduk++;
                        }?>
                        <script type="text/javascript">
                var jumlah = 0;
                function minus(obj)
                {
                    
                    var idButtonMinus = obj.id;
                    var temp = idButtonMinus.split("-");
                    var id = temp[1];
                    var idJumlah = "jumlah-" + id;
                    var test = parseFloat(document.getElementById(idJumlah).value);
                    if(test > 1){
                        document.getElementById(idJumlah).value--;
                        var jumlah = parseFloat(document.getElementById(idJumlah).value);
                        hitung(id, jumlah);
                    }
                    
                }
                function plus(obj)
                {
                    var idButtonPlus = obj.id;
                    var temp = idButtonPlus.split("-");
                    var id = temp[1];
                    var idJumlah = "jumlah-" + id;
                    var test = parseFloat(document.getElementById(idJumlah).value);
                    if(test < 10){
                        document.getElementById(idJumlah).value++;
                        var jumlah = parseFloat(document.getElementById(idJumlah).value);
                        hitung(id, jumlah);
                    }

                }
                function hitung(id, jumlah)
                {
                    var idHarga = "harga-" + id;
                    var harga = parseFloat(document.getElementById(idHarga).innerHTML);
                    var idHargaTerkali = "harga-terkali-" + id;
                    var hitung = harga * jumlah;
                    document.getElementById(idHargaTerkali).innerHTML = hitung;
                    if(<?= $counterProduk; ?> == 1){
                        document.getElementById('total').innerHTML = "Rp " + hitung;
                        document.cookie = "jumlah="+hitung;
                    }else{
                        hitungAll();
                    }
                }
                function hitungAll()
                {
                    var jumlah = 0;
                    <?php 
                        foreach ($_SESSION['keranjang'] as $ID_Produk => $jumlah) {
                            ?>
                                var idHargaTerkali = "harga-terkali-" + <?=$ID_Produk ?>;
                                jumlah += parseFloat(document.getElementById(idHargaTerkali).innerHTML);
                            <?php
                        }
                    ?>    
                    document.cookie = "jumlah="+jumlah;
                    var reverse = jumlah.toString().split('').reverse().join(''),
                    jumlah  = reverse.match(/\d{1,3}/g);
                    jumlah  = jumlah.join('.').split('').reverse().join('');
                    document.getElementById('total').innerHTML = "Rp " + jumlah;
                }
            </script>
            <tr class="table-row">
                            <td class="column-1"></td>
                            <td class="column-2"></td>
                            <td class="column-3"></td>
                            <td class="column-4"><h3>TOTAL :</h3></td>
                            <td class="column-5">
                                <div class="cart-totals-subtotal">
                                    <span class="cart-totals-total" id="total"><?=$total?></span>
                                </div>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>

            <div class="flex-w flex-sb-m p-t-25 p-b-25 bo8 p-l-35 p-r-60 p-lr-15-sm">
                <div class="flex-w flex-m w-full-sm">
                </div>

                <div class="size10 trans-0-4 m-t-10 m-b-10">
                    <!-- Button -->
                    <button class="flex-c-m sizefull bg1 bo-rad-23 hov1 s-text1 trans-0-4" name="checkout">
                        Checkout
                    </button>
                </div>
            </div>
        </div>
    </section>
</form>
  <?php
}else
{?>
    <br><br><br><br><br><br>
    <div style="text-align: center;">
        <div>
            <img src="img/keranjangkosong.png" width="400" height="200" >
        </div>
        <br><br><br>
        <div>
            <h5>Keranjang Anda Kosong , Silahkan Berbelanja</h5>
        </div>
    </div>
    <br><br><br>
    <br><br><br><br><br>
<?php }

    if (isset($_POST['checkout'])) {
        if(!isset($_SESSION["apps_user"]))
        {
            goToUrl('Login.php');
        }
        else
        {
         if (isset($_COOKIE['jumlah'])) {
            $jumlah2 =  $_COOKIE['jumlah']; 
            }
            $tanggal = date("Y-m-d");
            $conn->query("INSERT INTO keranjang (ID_User, tanggal ,total) VALUES ('$user', '$tanggal','$jumlah2')");

            $id_baru = $conn->insert_id;

            foreach ($_SESSION['keranjang'] as $ID_Produk => $jumlah) {
                $jumlahh = $_POST['form-'.$ID_Produk.''];
                $conn->query("INSERT INTO keranjang_barang(ID_Keranjang, ID_Produk , Jumlah) VALUES ('$id_baru', '$ID_Produk' ,'$jumlahh')");
            }
            unset($_SESSION['keranjang']);
            unset($_SESSION['notif']);
            unset($_COOKIE['jumlah']);

            goToUrl("checkout.php?ID_Keranjang=".$id_baru."");
        }   
    }  

doHTMLFooter(); 
?>