 <?php
session_start();
include('listFungsi.php');
doHtmlHeader('List Produk');
$conn = doConnect();
$user = getuserId();
$ID_Keranjang = $_GET['ID_Keranjang'];

$hasil = $conn->query("SELECT * FROM keranjang WHERE ID_Keranjang = '" .$ID_Keranjang. "'");
$Detail = mysqli_fetch_assoc($hasil);

$folder   ="img/bukti_pembayaran/";
if(isset($_POST['tambah'])){
  if(!empty($_FILES['gambar']['tmp_name']))
  {
    $jenis_gambar = $_FILES['gambar']['type'];
    if($jenis_gambar=="image/jpeg" || $jenis_gambar=="image/jpg" || $jenis_gambar=="image/gif" || $jenis_gambar=="image/png"){
      $gambar =$folder.basename($_FILES['gambar']['name']);
      if(move_uploaded_file($_FILES['gambar']['tmp_name'], $gambar)){
        $conn->query("UPDATE transaksi SET Bukti_Pembayaran ='$gambar',Status = 4 WHERE ID_Keranjang = $ID_Keranjang");
        echo "<script type='text/javascript'>alert('Sukses');window.location.href='previewPembayaran.php?ID_Keranjang=".$ID_Keranjang."';</script>";
      }
      else{
        echo "<script type='text/javascript'>alert('Gagal');</script>";
      }
    }
  }
}

?>
<br>
<br>
<br>
<br>
<br>
<div class="container">
	<div class="row">
		<div class="col-sm-12">
			<h2><center>Upload bukti pembayaran 
			</center></h2><br>	
			<form action="" method="post" enctype="multipart/form-data">
				<input type="hidden" name="ID_Keranjang" value="<?=$ID_Keranjang ?>">
				<div class="form-group" >	
					<center>Total Pembelian : <h5>Rp.<?=number_format($Detail['total']);?></h5></center>
					<br>
					<center>Terima kasih, Anda telah berhasil melakukan checkout<br>Silahkan transfer kesalah satu nomor rekening dibawah ini</center>
				</div>
				</div>

			<div class="row">
				<div class="col-sm-10 col-md-4 p-b-30 m-l-r-auto">
					<!-- Block3 -->
					<div class="block3">
							<center><img src="img/BRI.jpg" alt="IMG-BLOG" height="150px" width="200px"></center>
						<div class="block3-txt p-t-14">
							<h6 class="p-b-7">
								Nama Bank : BRI
							</h6>
							<h6>
								Alamat : Jl.Sisingamangaraja No. 97 Tapanuli Utara, Sumatera Utara	- 22411	
								Telepon : (0633) 21445
							</h6>
							<h6>
								NO Rekening : 119000000
							</h6>
						</div>
					</div>
				</div>

				<div class="col-sm-10 col-md-4 p-b-30 m-l-r-auto">
					<!-- Block3 -->
					<div class="block3">
							<center><img src="img/BNI.png" alt="IMG-BLOG" height="150px" width="200px"></center>
						<div class="block3-txt p-t-14">
							<h6 class="p-b-7">
								Nama Bank : Bank BNI
								</h6>
								<h6>
								Alamat : JL. Sisimangaraja NO.82 Tapanuli Utara, Sumatera Utara	- 22411	
								Telepon : (0632)21878
							</h6>
							<h6>
								NO Rekening : 165000001
							</h6>
						</div>
					</div>
				</div>

				<div class="col-sm-10 col-md-4 p-b-30 m-l-r-auto">
					<!-- Block3 -->
					<div class="block3">
							<center><img src="img/sumut.jpg" alt="IMG-BLOG" height="150px" width="200px"></center>
						<div class="block3-txt p-t-14">
							<h6 class="p-b-7">
								Nama Bank : Bank Sumut
							</h6>
							<h6>
								Alamat : JL. Sisimangaraja NO.82
								Tapanuli Utara, Sumatera Utara	- 22411	
								Telepon : (0632)21878
							</h6>
							<h6>
								NO Rekening : 1240900987
							</h6>
						</div>
					</div>
				</div>

					<div class="container col-md-4">
					
						<input type="file" name="gambar" accept="img/*" class="form-control" >
						<br>
						<center><button class="btn btn-large btn-primary" name="tambah" type="submit">Saya Sudah Membayar</button></center>
					</div>
					
	</div>
</div>
</form>
</div>
</div>

</br>
</br>
</br>
<?php 
	doHtmlFooter();
?>