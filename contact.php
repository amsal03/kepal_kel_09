
<?php
include('listFungsi.php');
doHtmlHeader('Home');
?>
<br><br><br>
	
	<!-- content page -->
	<section class="bgwhite p-t-66 p-b-60">
		<div class="container">
			<div class="row">
				<div class="col-md-6 p-b-30">
					<div class="p-r-20 p-r-0-lg">
						<div ><iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d419.10809506481775!2d98.95118690000001!3d2.0556813000000003!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x302e6c3c1c400a91%3A0x7cc14c20c97eff71!2sGitar+Sipoholon!5e0!3m2!1sen!2sid!4v1544340706125" width="500" height="450" frameborder="0" style="border:0" allowfullscreen></iframe></div>
						
					</div>
				</div>

				<div class="col-md-6 p-b-30">
					 <form action = "actionContact.php" method="POST" >
                    <div class="col-md-3" >
                        <div class="form-control-feedback">
                                <span class="text-danger align-middle">
                                    <!-- Put name validation error messages here -->
                                </span>
                        </div>
                    </div>
						<h4 class="m-text26 p-b-36 p-t-15">
							Kirimkan pesan Anda kepada kami 
						</h4>

						<div>
							<input class="form-control" type="text" name="namalengkap" placeholder="Nama Lengkap" required="">
						</div>
						<br>
						<div>
							<input class="form-control" type="text" name="email" placeholder="Alamat Email" required="">
						</div>
						<br>
						<div>
							<input class="form-control" type="number" name="nomortelepon" placeholder="Nomor Telepon" required="">
						</div>
						<br>
						
						<textarea class="form-control" name="pesan" placeholder="Pesan Anda" rows="6" required=""></textarea>

						<br>
						<div class="w-size25">
							<!-- Button -->
							<button class="flex-c-m size2 bg1 bo-rad-23 hov1 m-text3 trans-0-4">
								Kirim
							</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</section>

<?php 
doHtmlFooter();
?>
