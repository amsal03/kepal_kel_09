<?php
include('listFungsi.php');
doHtmlHeader('akun');
$db = doConnect();
$user = getuserId();
$hasil = $db->query("SELECT * FROM user WHERE ID_User = '" .$user. "'");
$UserDetail = mysqli_fetch_assoc($hasil);

?>


<hr><br><br><br>
<div class="container bootstrap snippet">
    <div class="row">
  		
    </div>
    <div class="row">
  		<div class="col-sm-3">     
      		<div class="text-center">
      			<br>
		        <?php
		        	if($UserDetail['Foto'] != NULL)
		        	{
		        		echo '<img src="'.$UserDetail['Foto'].'" class="avatar img-circle img-thumbnail" alt="avatar">';
		        	}
		        	else {
		        		echo '<img src="img/avatar.png" class="avatar img-circle img-thumbnail" alt="avatar">';		
		        	}
		        ?>
      		</div>
        </div><!--/col-3-->
    	<div class="col-sm-9">
          <div class="tab-content">
            <div class="tab-pane active" id="home">
            <br>
	          <table class="table">
	          	<tr class="table-head">
	          		<th>Username</th>
	          		<td>:</td>
	          		<td><?php echo $UserDetail['Username'];?></td>
	          	</tr>
	          	<tr class="table-head">
	          		<th>Nama Lengkap</th>
	          		<td>:</td>
	          		<td><?php echo $UserDetail['Nama'];?></td>
	          	</tr>
	          	<tr class="table-head">
	          		<th>Email</th>
	          		<td>:</td>
	          		<td><?php echo $UserDetail['Email'];?></td>
	          	</tr>
	          	<tr class="table-head">
	          		<th>Nomor Telepon</th>
	          		<td>:</td>
	          		<td><?php echo $UserDetail['NoTelepon'];?></td>
	          	</tr>
	          </table>
              	<a href="updateUser.php"><button class="btn btn-sm btn-primary" type="submit" ><i class="glyphicon glyphicon-edit"></i> Edit Akun</button></a>
              <hr>
            </div><!--/tab-pane-->
           </div><!--/tab-pane-->
          </div><!--/tab-content-->
	</div><!--/col-9-->
</div><!--/row-->
    <br><br>
                                                      
<?php 
doHtmlFooter();
?>
<script type="text/javascript">
	
	$(document).ready(function() {

    
    var readURL = function(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('.avatar').attr('src', e.target.result);
            }
    
            reader.readAsDataURL(input.files[0]);
        }
    }
    

    $(".file-upload").on('change', function(){
        readURL(this);
    });
});
</script>