<?php
include('listFungsi.php');
doHtmlHeader('akun');
$db = doConnect();
$user = getuserId();
$hasil = $db->query("SELECT * FROM user WHERE ID_User = '" .$user. "'");
$UserDetail = mysqli_fetch_assoc($hasil);

?>
<hr><br><br><br>
<form method="POST" action="actionUpdateUser.php" enctype="multipart/form-data">
<div class="container bootstrap snippet">
    <div class="row">
      
    </div>
    <div class="row">
      <div class="col-sm-3">     
          <div class="text-center">
            <br>
            <?php
              if($UserDetail['Foto'] != NULL)
              {
                echo '<img src="'.$UserDetail['Foto'].'" class="avatar img-circle img-thumbnail" alt="avatar" name="gambar">';
              }
              else {
                echo '<img src="img/avatar.png" class="avatar img-circle img-thumbnail" alt="avatar" name="gambar">';   
              }
            ?>
          </div>
          <br>
        </div><!--/col-3-->
      <div class="col-sm-9">
          <div class="tab-content">
            <div class="tab-pane active" id="home">
            <br>
            <table class="table">
              <tr class="table-head">
                <th>Nama Lengkap</th>
                <td>:</td>
                <td><input type="text" class="form-control" name="name" id="name" value="<?php echo $UserDetail['Nama'];?>" required></td>
              </tr>
              <tr class="table-head">
                <th>Email</th>
                <td>:</td>
                <td><input type="email" class="form-control" id="email" name ="email" value="<?php echo $UserDetail['Email'];?>" required></td>
              </tr>
              <tr class="table-head">
                <th>Username</th>
                <td>:</td>
                <td><input type="text" class="form-control" name="username" id="username" value="<?php echo $UserDetail['Username'];?>" required></td>
              </tr>
              <tr class="table-head">
                <th>Nomor Telepon</th>
                <td>:</td>
                <td><input type="number" class="form-control" name="NoTelepon" id="NoTelepon" value="<?php echo $UserDetail['NoTelepon'];?>" required></td>
              </tr>
              <tr class="table-head">
                <th>Password lama</th>
                <td>:</td>
                <td><input type="password" class="form-control" name="passwordlama" id="passwordlama" value="" required=""></td>
              </tr>
              <tr class="table-head">
                <th>Password baru</th>
                <td>:</td>
                <td><input type="password" class="form-control" name="passwordbaru" id="passwordbaru" value="" required=""></td>
              </tr>
              <tr class="table-head">
                <th>Foto Profil</th>
                <td>:</td>
                <td><input type="file" name="gambar" class="file-upload" required=""></td>
              </tr>
            </table>
                <button class="btn btn-sm btn-success " type="submit" onclick="swal();"><i class="glyphicon glyphicon-ok-sign"></i>Save</button>
                <button class="btn btn-sm btn-danger " type="submit" name="batal"><i class="glyphicon glyphicon-ok-sign"></i>Batal</button>
              <hr>
            </div><!--/tab-pane-->
           </div><!--/tab-pane-->
          </div><!--/tab-content-->
  </div><!--/col-9-->
</div><!--/row-->
    <br><br>
  
</form>

                                                      
<?php 
doHtmlFooter();

?>
<script type="text/javascript">
	
	$(document).ready(function() {

    
    var readURL = function(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('.avatar').attr('src', e.target.result);
            }
    
            reader.readAsDataURL(input.files[0]);
        }
    }
    

    $(".file-upload").on('change', function(){
        readURL(this);
    });
});

  function swal(){
    swal({
          title: "Are you sure?",
          text: "Once deleted, you will not be able to recover this imaginary file!",
          icon: "warning",
          buttons: true,
          dangerMode: true,
        })
        .then((willDelete) => {
          if (willDelete) {
            swal("Poof! Your imaginary file has been deleted!", {
              icon: "success",
            });
          } else {
            swal("Your imaginary file is safe!");
          }
        });
  }
</script>