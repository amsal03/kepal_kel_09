<!DOCTYPE html>
<html lang="en">
<head>
    <title>Gitar Sipoholon</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
<!--===============================================================================================-->
    <link rel="icon" type="image/png" href="images/icons/favicon.png"/>
<!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="vendor/bootstrap/css/bootstrap.min.css">
<!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="fonts/font-awesome-4.7.0/css/font-awesome.min.css">
<!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="fonts/themify/themify-icons.css">
<!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="fonts/Linearicons-Free-v1.0.0/icon-font.min.css">
<!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="fonts/elegant-font/html-css/style.css">
<!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="vendor/animate/animate.css">
<!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="vendor/css-hamburgers/hamburgers.min.css">
<!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="vendor/animsition/css/animsition.min.css">
<!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="vendor/select2/select2.min.css">
<!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="vendor/daterangepicker/daterangepicker.css">
<!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="vendor/slick/slick.css">
<!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="dist/sweetalert2.min.css">
<!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="vendor/lightbox2/css/lightbox.min.css">
<!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="css/util.css">
    <link rel="stylesheet" type="text/css" href="css/main.css">
<!--===============================================================================================-->
</head>
<body>
    <!-- Header -->
    <header class="header2">
        <!-- Header desktop -->
        <div class="container-menu-header">
            <div class="wrap_header">
                <!-- Logo -->
                <a href="index.php" class="logo">
                    <img src="img/logos.png" alt="IMG-LOGO">
                </a>
                <!-- Header Icon -->
                <div class="header-icons">
                    <!-- Menu -->
                <div class="wrap_menu">
                    <nav class="menu">
                        <ul class="main_menu">
                            <li>
                                <a href="index.php">Beranda</a>
                            </li>

                            <li>
                                <a href="produks.php">Produk</a>
                            </li>

                            <li>
                                <a href="tentang.php">Tentang</a>
                            </li>

                            <li>
                                <a href="contact.php">Kontak</a>
                            </li>
                        </ul>
                    </nav>
                </div>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <div class="header-wrapicon2">
                        <img src="img/icons/icon-header-02.png" class="header-icon1 js-show-header-dropdown" alt="ICON">
                        <span class="header-icons-noti">0</span>
                    
                    
                        <?php
                            if(!isset($_SESSION['apps_user'])){
                               echo '<div class="header-cart header-dropdown">
                            <div class="header-cart-buttons">
                                <div class="header-cart-wrapbtn">
                                    <!-- Button -->
                                    <a href="cart.php" class="flex-c-m size1 bg4 bo-rad-20 hov1 s-text1 trans-0-4">
                                        Cart
                                    </a>
                                </div>

                                <div class="header-cart-wrapbtn">
                                    <!-- Button -->
                                    <a href="checkout.php" class="flex-c-m size1 bg4 bo-rad-20 hov1 s-text1 trans-0-4">
                                        Checkout
                                    </a>
                                </div>
                            </div>
                        </div>';
                        }
                    ?>
                </div>
                   <span class="linedivide1"></span>
                    <div class="header-wrapicon2">
                        <img src="img/icons/icon-header-01.png" class="header-icon1 js-show-header-dropdown" alt="ICON">      

                        <?php
                            if(!isset($_SESSION['apps_user'])){
                               echo '<div class="header-cart header-dropdown">
                            <div class="header-cart-buttons">
                                <div class="header-cart-wrapbtn">
                                    <!-- Button -->
                                    <a href="Login.php" class="flex-c-m size1 bg4 bo-rad-20 hov1 s-text1 trans-0-4">
                                        Login
                                    </a>
                                </div>

                                <div class="header-cart-wrapbtn">
                                    <!-- Button -->
                                    <a href="Register.php" class="flex-c-m size1 bg4 bo-rad-20 hov1 s-text1 trans-0-4">
                                        Daftar
                                    </a>
                                </div>
                            </div>
                        </div>'; 
                            }else{
                                echo '<div class="header-cart header-dropdown">
                                        <div class="header-cart-buttons">
                                            <div class="header-cart-wrapbtn">
                                                <!-- Button -->
                                                <a href="account.php" class="flex-c-m size1 bg1 bo-rad-20 hov1 s-text1 trans-0-4">
                                                    Akun
                                                </a>
                                            </div>
                                            <div class="header-cart-wrapbtn">
                                                <!-- Button -->
                                                <a href="actionLogout.php" class="flex-c-m size1 bg1 bo-rad-20 hov1 s-text1 trans-0-4">
                                                    Log Out
                                                </a>
                                            </div>
                                        </div>
                                    </div>';
                            }
                        ?>
                        
                    </div>
                </div>
            </div>
        </div>
        <!--===============================================================================================-->
    <script type="text/javascript" src="vendor/jquery/jquery-3.2.1.min.js"></script>
<!--===============================================================================================-->
    <script type="text/javascript" src="vendor/animsition/js/animsition.min.js"></script>
<!--===============================================================================================-->
    <script type="text/javascript" src="vendor/bootstrap/js/popper.js"></script>
    <script type="text/javascript" src="vendor/bootstrap/js/bootstrap.min.js"></script>
<!--===============================================================================================-->
    <script type="text/javascript" src="vendor/select2/select2.min.js"></script>
    <script type="text/javascript">
        $(".selection-1").select2({
            minimumResultsForSearch: 20,
            dropdownParent: $('#dropDownSelect1')
        });
    </script>
<!--===============================================================================================-->
    <script type="text/javascript" src="vendor/slick/slick.min.js"></script>
    <script type="text/javascript" src="js/slick-custom.js"></script>
<!--===============================================================================================-->
    <script type="text/javascript" src="vendor/countdowntime/countdowntime.js"></script>
<!--===============================================================================================-->
    <script type="text/javascript" src="vendor/lightbox2/js/lightbox.min.js"></script>
<!--===============================================================================================-->
    <script type="text/javascript" src="vendor/sweetalert/sweetalert.min.js"></script>
    <script type="text/javascript">
        $('.block2-btn-addcart').each(function(){
            var nameProduct = $(this).parent().parent().parent().find('.block2-name').html();
            $(this).on('click', function(){
                swal(nameProduct, "is added to cart !", "success");
            });
        });

        $('.block2-btn-addwishlist').each(function(){
            var nameProduct = $(this).parent().parent().parent().find('.block2-name').html();
            $(this).on('click', function(){
                swal(nameProduct, "is added to wishlist !", "success");
            });
        });
    </script>

<!--===============================================================================================-->
    <script src="js/main.js"></script>

    </header>