<?php
session_start();
include('listFungsi.php');
doHtmlHeader('Cart');
$conn = doConnect();
$user = getuserId();

$hasil = $conn->query("SELECT * FROM transaksi WHERE ID_User = '" .$user. "' ORDER BY tanggal DESC");

if( $hasil->num_rows >= 0 ){
?>
<br>
<br>
<br>
<br>
<br>
<br>
<div class="container">
    <table class="table">
        <tr style="background:#f0f0f0 ">
            <th>NO</th>
            <th colspan="3">Produk</th>
            <th>Status Pembelian</th>
            <th></th>
        </tr>
        <?php $no = 1; 
            while($detail = $hasil->fetch_assoc()){ 
            $query = $conn->query("SELECT * FROM keranjang_barang JOIN produk ON keranjang_barang.ID_Produk = produk.ID_Produk
                WHERE keranjang_barang.ID_Keranjang = '".$detail['ID_Keranjang']."'");
        ?>
        <tr>
            <td><?=$no; ?></td>
            <td colspan="3">
                <table class="table">
                    <?php
                        $nomor = 1;
                        while ($details  = $query->fetch_assoc() ) {
                    ?>
                        <tr>
                            <td><?=$nomor;?></td>
                            <td><?='<img src="img/'.$details['Gambar'].'" height="50px">';?></td>
                            <td><?php echo $details['Nama']; ?> X <?php echo $details['Jumlah'];?><td>
                            <td>Order ID : #<?php echo $detail['ID_Transaksi']; ?><td>
                        </tr>
                    <?php 
                       $nomor++; }
                    ?>
                    
                </table>
            </td>
            <td>
               <?php 
                if ($detail['Status'] == 0) {
                    echo "Pembayaran belum dilakukan";
                }else if($detail['Status'] == 1){
                    echo "Pembelian Sedang di proses";
                }else if ($detail['Status'] == 2) {
                    echo "Pembelian ditolak";
                }else if ($detail['Status'] == 3) {
                    echo "Barang telah dikirim";
                }else if($detail['Status'] == 4){
                    echo "Menunggu konfirmasi";
                }
               ?>
            </td>
            <td><?php 
                if ($detail['Status'] == 0) {
                    echo '<a href="Pembayaran.php?ID_Keranjang='.$detail["ID_Keranjang"].'" class="btn btn-warning btn-sm">Bayar sekarang</a>';
                }else{?>
                <a href="detailPengiriman.php?ID_Transaksi=<?=$detail['ID_Transaksi'];?>"  class="btn btn-primary btn-sm">Detail</a>
                <?php }?>
            </td>
        </tr>
        <?php $no++; } ?>
    </table>
</div>

<?php 
}
else{
   ?>
   <br><br><br><br><br><br>
    <div style="text-align: center;">
        <div>
            <img src="img/keranjangkosong.png" width="400" height="200" >
        </div>
        <br><br><br>
        <div>
            <h5>Checkout Kosong , Silahkan Berbelanja</h5>
        </div>
    </div>
    <br><br><br>
    <br><br><br><br><br>
   <?php 
}
doHTMLFooter(); 
?>