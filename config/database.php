<?php

class Database
{

	public $connection;

	//credential
	private $host = "localhost";
	private $dbname = "wetarsip";
	private $username = "root";
	private $password = "";

	public function getConnection()
	{
		$this->connection = null;

		try {
			$this->connection = new PDO("mysql:host=" . $this->host . ";dbname=" . $this->dbname, $this->username, $this->password);
		} catch (PDOException $exception) {
			echo "Connection error: " . $exception->getMessage();
		}

		return $this->connection;
	}
}