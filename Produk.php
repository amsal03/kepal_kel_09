<?php
include('listFungsi.php');
doHtmlHeader('List Produk');
$db = doConnect();
$user = getuserId();

if(isset($_GET['ID_Produk'])){
    $ID_Produk = $_GET['ID_Produk']; 
}


    $hasil = $db->query("SELECT * FROM Produk WHERE ID_Produk = '" .$ID_Produk. "'");
    $produkDetail = mysqli_fetch_array($hasil);

?>
<br><br><br>
<form method="POST">
    <div class="container bgwhite p-t-35 p-b-80">
        <div class="flex-w flex-sb">
            <div class="w-size13 p-t-30 respon5">
                <div class="wrap-slick3 flex-sb flex-w">
                    <div class="wrap-slick3-dots"></div>

                    <div class="slick3">    
                        <?php echo'<div class="item-slick3" data-thumb="img/'.$produkDetail['Gambar'].'">'?>
                            <div class="wrap-pic-w border">
                                <?='<img src="img/'.$produkDetail['Gambar'].'" alt="IMG-PRODUCT">'?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="w-size14 p-t-30 respon5">
                <h4 class="product-detail-name m-text16 p-b-13">
                    <?php echo $produkDetail['Nama'];?>
                </h4>

                <span class="m-text17">
                    Rp.<?php echo number_format($produkDetail['Harga']);?>
                </span>

                <p class="s-text8 p-t-10">
                    <?php echo $produkDetail['Deskripsi'];?>
                </p>

                <!--  -->
                <div class="p-t-33 p-b-60">
                    <div class="flex-r-m flex-w p-t-10">
                        <div class="w-size16 flex-m flex-w">
                            <br><br><br><br><br><br><br><br><br>
                            <div class="btn-addcart-product-detail size9 trans-0-4 m-t-10 m-b-10">
                                <button class="flex-c-m sizefull bg1 bo-rad-23 hov1 s-text1 trans-0-4" name="masuk">
                                    Add to Cart
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>
 <?php 
doHtmlFooter();
    if (isset($_POST['masuk'])) {
        if(isset($_SESSION['notif'][$ID_Produk]))
        {
            $_SESSION['notif'][$ID_Produk] += 1;
        }
        else
        {
            $_SESSION['notif'][$ID_Produk] = 1;
        }

    if(isset($_SESSION['keranjang'][$ID_Produk]))
    {
        $_SESSION['keranjang'][$ID_Produk] = 1;
    }
    else
    {
        $_SESSION['keranjang'][$ID_Produk] = 1;
    }
    ?>
    <script type="text/javascript">
        swal("<?php echo $produkDetail['Nama'];?>", "Ditambahkan ke keranjang", "success");
    </script>
    <script type="text/javascript">window.location = 'Produk.php?ID_Produk=<?=$data['ID_Produk']?>'</script>
    <?php
    
}