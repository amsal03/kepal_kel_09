<?php 
session_start();
include('listFungsi.php');
doHtmlHeader('Pembayaran');
$conn = doConnect();
$user = getuserId();
$ID_Keranjang = $_GET['ID_Keranjang'];

$de = $conn->query("SELECT * FROM transaksi WHERE ID_Keranjang = '".$ID_Keranjang."'");
$detail = $de->fetch_assoc();

$des = $conn->query("SELECT * FROM keranjang WHERE ID_Keranjang = '".$ID_Keranjang."'");
$details = $des->fetch_assoc();

?>
<br>
<br>
<br>
<br>
<br>
<br>

<div class="container">
            <div id="confirm" class="thankyou">
                <div class="col-md-12 pt-3">
                    <h4>Terimakasih. Pembelian anda akan segera di proses.</h4>
                    <br>
                    <div class="lumise-order-sumary">
                        Order ID  : <strong>#<?php echo $detail['ID_Transaksi'];?></strong><br>
                        Date : <strong><?php echo $detail['tanggal'];?></strong><br>
						Status : <strong>
							<?php if ($detail['Status'] == 0) {
								echo "Menunggu";
							}elseif ($detail['Status'] == 1) {
								echo "Barang telah di proses";
							}elseif ($detail['Status'] == 2) {
								echo "Pembayaran di tolak, hubungi kami untuk informasi lebih lanjut";
							}elseif ($detail['Status'] == 3 ) {
								echo "Barang telah dikirim";
							} ?></strong><br>
                        Total  : <strong>Rp. <?php echo number_format($details['total']);?></strong><br>
                        Bukti Pembayaran : <br><br><img class="img-palaroid" src="<?=$detail['Bukti_Pembayaran'];?>" style="width:250px; height: 200px;  margin-right:10px;"/>
                    </div>
                    <br><br>
                    <h4><?php echo "Detail pembelian"?></h4><br>
                    <div class="table">
                        <table class="table">
                            <thead style="background:#f0f0f0 ">
                                <tr>
                                    <th><?php echo 'Product Name'; ?></th>
                                    <th><?php echo 'Total'; ?></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $query1=$conn->query("SELECT * FROM produk P INNER JOIN Keranjang_barang K ON K.ID_Produk = P.ID_Produk Where ID_Keranjang = ".$ID_Keranjang."") ;
                                while($data1=$query1->fetch_assoc()){ ?>
                                	<tr>
                                    <td><span class="product-title"><?php echo $data1['Nama'];?></span> x <?php echo $data1['Jumlah'];?></td>                                    
                                    <td>Rp. <?php echo number_format($data1['Harga'] * $data1['Jumlah']) ;?>,-</td>
                                </tr>

                                <?php }?>
                                <tr style="background:#f0f0f0 ">
                                    <td><strong> Grand Total</strong></td>
                                    <td><strong>Rp. <?php echo number_format($details['total']); ?>,-</strong></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <br>
					<div class="mt-30"></div>
                    <h4>Customer details</h4>
                    <br>
                    <div class="table">
                        <table class="table">
                            
                            <tbody>
                                <tr>
                                    <td style="background:#f0f0f0 "><strong><?php echo 'Nama Penerima'; ?></strong></td>
                                    <td><?php echo $detail['Nama_penerima'];?></td>
                                </tr>
                                <tr>
                                    <td style="background:#f0f0f0 "><strong><?php echo ' Nomor Telepon'; ?></strong></td>
                                    <td><?php echo $detail['NoTelepon'];?></td>
                                </tr>
                            </tbody>
                        </table>
						<div class="mt-30"></div>
                        <div class="lumise-billing-details">
							<br><h4><?php echo 'Alamat Pengiriman'; ?></h4><br>
                            <?php echo $detail['Alamat_Pengiriman'];?><br>
                            <?php echo $detail['Kota'];?><br>
                            <?php echo $detail['Provinsi'];?>
                            <?php echo $detail['Kode_Pos'];?>
                        </div>
                    </div>
					<div class="mt-30"></div>
                    <div class="form-actions">
                    	<br><br>
                        <a href="produks.php" class="btn btn-large btn-primary">Lanjut Belanja</a>
                    </div>
                </div>
            </div>
            <br><br><br><br>
        </div>
    </div>
<?php 
	doHtmlFooter();
?>