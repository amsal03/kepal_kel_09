<?php
include('listFungsi.php');
doHtmlHeader('Home');
$db = doConnect();
$user = getuserId();
?>
		

	<!-- Title Page -->
	<!--section class="bg-title-page p-t-40 p-b-50 flex-col-c-m" style="background-color: #F0F8FF;">
		<h1 class="1-text2 t-center" style="color:black font: 150px">
			Gitar Sipoholon
		</h1>
	</section>

	<!- content page -->
<br><br><br><br>
	<section class="bgwhite p-t-66 p-b-38">
		<div class="container">
			<div class="row">
				<div class="col-md-4 p-b-30">
					<div class="hov-img-zoom">
						<img src="img/blog1.png" alt="IMG-ABOUT">
					</div>
				</div>
				<div class="col-md-8 p-b-30">
					<h3 class="m-text26 p-t-15 p-b-16">
						Our story
					</h3>

					<p class="p-b-28"

					style="color: black font: 25px font style: sans serif">
						Gitar adalah sebuah alat musik berdawai yang dimainkan dengan cara dipetik, umumnya menggunakan jari maupun plektrum. Gitar terbentuk atas sebuah bagian tubuh pokok dengan bagian leher yang padat sebagai tempat senar yang umumnya berjumlah enam didempetkan. Gitar secara tradisional dibentuk dari berbagai jenis kayu dengan senar yang terbuat dari nilon maupun baja. Beberapa gitar modern dibuat dari material polikarbonat. 
						Satu lagi hasil karya kebudayaan anak negeri menembus pasar Amerika. Kali ini datang dari industri kerajinan rakyat Sipoholon, Kabupaten Tapanuli Utara, Sumatera Utara. Gitar Sipoholon. Menurut penjelasan geografisnya sebenarnya Sipoholon adalah nama salah satu kecamatan di Kabupaten Tapanuli Utara. Sipoholon terkenal akan produk gitar akustiknya yang sudah menasional, bahan mendunia karena kualitas suaranya yang nyaring.  Gitar yang memiliki kayu berkualitas dan suara cukup nyaring ini telah berhasil menembus pasar Amerika Serikat. Gitar buatan Sipoholon, Tapanuli Utara, Sumetara Utara,memang sudah lama mengukir kebanggaan dalam sejarah. Bahkan boleh disebut sudah legendaries. Meski sudah sering dipublikasikan beberapa media cetak, tapi detailnya kurang tanpa mengungkap proses alih generasi yang terjadi saat ini.
					</p>

					<div class="bo13 p-l-29 m-l-9 p-b-10">
						<p class="p-b-11">
							Bapak Hutagalung adalah pembuat Gitar Sipoholon, beliau adalah pionir pembuatan Gitar Sipoholon, dengan bermodalkan keterampilan musik beliau memberanikan diri membuat sendiri Gitar yang sekarang menjadi Gitar Legendaris di tanah Batak.
						</p>

						<span class="s-text7">
							- Gitar Sipoholon
						</span>
					</div>
				</div>
			</div>
		</div>
	</section>

<?php 
doHtmlFooter();
?>



	<!-- Back to top -->
	<div class="btn-back-to-top bg0-hov" id="myBtn">
		<span class="symbol-btn-back-to-top">
			<i class="fa fa-angle-double-up" aria-hidden="true"></i>
		</span>
	</div>

	<!-- Container Selection -->
	<div id="dropDownSelect1"></div>
	<div id="dropDownSelect2"></div>



<!--===============================================================================================-->
	<script type="text/javascript" src="vendor/jquery/jquery-3.2.1.min.js"></script>
<!--===============================================================================================-->
	<script type="text/javascript" src="vendor/animsition/js/animsition.min.js"></script>
<!--===============================================================================================-->
	<script type="text/javascript" src="vendor/bootstrap/js/popper.js"></script>
	<script type="text/javascript" src="vendor/bootstrap/js/bootstrap.min.js"></script>
<!--===============================================================================================-->
	<script type="text/javascript" src="vendor/select2/select2.min.js"></script>
	<script type="text/javascript">
		$(".selection-1").select2({
			minimumResultsForSearch: 20,
			dropdownParent: $('#dropDownSelect1')
		});

		$(".selection-2").select2({
			minimumResultsForSearch: 20,
			dropdownParent: $('#dropDownSelect2')
		});
	</script>
<!--===============================================================================================-->
	<script src="js/main.js"></script>

?>
